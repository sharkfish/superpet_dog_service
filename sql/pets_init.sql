﻿/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50610
Source Host           : localhost:3306
Source Database       : superpet

Target Server Type    : MYSQL
Target Server Version : 50610
File Encoding         : 65001

Date: 2018-01-28 19:03:28
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for p_key_value
-- ----------------------------
DROP TABLE IF EXISTS `p_key_value`;
CREATE TABLE `p_key_value` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pkey` varchar(50) DEFAULT NULL,
  `pvalue` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of p_key_value
-- ----------------------------
INSERT INTO `p_key_value` VALUES ('1', 'IMG_PATH_PREFIX', 'https://yourDomainName');

-- ----------------------------
-- Table structure for p_pets
-- ----------------------------
DROP TABLE IF EXISTS `p_pets`;
CREATE TABLE `p_pets` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `petNo` varchar(10) NOT NULL,
  `petName` varchar(64) NOT NULL COMMENT '名称',
  `birthday` varchar(10) DEFAULT NULL COMMENT '生日',
  `avatar` varchar(100) NOT NULL COMMENT '头像',
  `sex` int(1) DEFAULT NULL COMMENT '性别：-男，0-女',
  `isNeuter` int(1) DEFAULT '0' COMMENT '绝育：1-已绝育，0-未绝育',
  `weight` double(5,2) DEFAULT NULL COMMENT '体重，单位kg',
  `feature` varchar(255) DEFAULT NULL COMMENT '一句描述：爱吃啥，有啥特点',
  `userid` bigint(20) NOT NULL COMMENT '宠物主人id',
  `userNickName` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '宠物主人昵称',
  `totalEnergy` bigint(20) DEFAULT '0' COMMENT '历史运动总值',
  `constellation` varchar(20) DEFAULT NULL COMMENT '星座',
  `updateTime` varchar(23) DEFAULT NULL COMMENT '更新时间',
  `breed` varchar(100) DEFAULT NULL COMMENT '品种',
  `activeFlag` int(1) DEFAULT '1' COMMENT '记录是否可用：1-可用，0-不可用',
  `levelNo` int(3) DEFAULT '1' COMMENT '等级',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;


-- ----------------------------
-- Table structure for p_pet_card
-- ----------------------------
DROP TABLE IF EXISTS `p_pet_card`;
CREATE TABLE `p_pet_card` (
  `id` bigint(20) NOT NULL,
  `userid` bigint(20) DEFAULT NULL COMMENT '卡的主人',
  `masterName` varchar(100) DEFAULT NULL COMMENT '主人名称',
  `petName` varchar(100) DEFAULT NULL COMMENT '宠物名称',
  `cardPic` varchar(255) DEFAULT NULL COMMENT '卡的图片',
  `cardContent` varchar(255) DEFAULT NULL COMMENT '卡的祝福语',
  `updateTime` varchar(23) DEFAULT NULL,
  `activeFlag` int(1) DEFAULT '1' COMMENT '记录是否可用：1-可用，0-不可用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for p_pet_feed_log
-- ----------------------------
DROP TABLE IF EXISTS `p_pet_feed_log`;
CREATE TABLE `p_pet_feed_log` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `userid` bigint(20) NOT NULL COMMENT '遛狗的用户id',
  `petid` bigint(20) NOT NULL COMMENT '被遛的狗id',
  `updateTime` varchar(23) NOT NULL,
  `feedVal` int(11) NOT NULL DEFAULT '0' COMMENT '本次遛狗获得的运动值',
  `petUserid` bigint(20) NOT NULL COMMENT '被遛的狗的主人id',
  `feedDate` varchar(10) DEFAULT NULL COMMENT '遛狗日',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for p_pet_feed_total
-- ----------------------------
DROP TABLE IF EXISTS `p_pet_feed_total`;
CREATE TABLE `p_pet_feed_total` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `userid` bigint(20) NOT NULL COMMENT '遛狗的用户id',
  `petid` bigint(20) NOT NULL COMMENT '被遛的狗id',
  `updateTime` varchar(23) NOT NULL,
  `feedVal` bigint(20) NOT NULL DEFAULT '0' COMMENT '宠物从该用户获得的运动总值',
  `petUserid` bigint(20) NOT NULL COMMENT '被遛的狗的主人id',
  `picLiker` int(1) NOT NULL DEFAULT '0' COMMENT '点赞写真集：1-已赞，0-未赞',
  `isCollect` int(1) NOT NULL DEFAULT '0' COMMENT '是否收入狗圈：1-已收入，0-未收入',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for p_pet_house
-- ----------------------------
DROP TABLE IF EXISTS `p_pet_house`;
CREATE TABLE `p_pet_house` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `userid` bigint(20) NOT NULL COMMENT '收藏狗的用户id',
  `petid` bigint(20) NOT NULL COMMENT '被收藏的狗id',
  `petUserid` bigint(20) NOT NULL COMMENT '被收藏的狗的主人id',
  `activeFlag` int(1) NOT NULL DEFAULT '1' COMMENT '记录是否可用：1-可用，0-不可用',
  `updateTime` varchar(23) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;


-- ----------------------------
-- Table structure for p_pet_pic
-- ----------------------------
DROP TABLE IF EXISTS `p_pet_pic`;
CREATE TABLE `p_pet_pic` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `petid` bigint(20) NOT NULL,
  `picPath` varchar(255) NOT NULL COMMENT '图片路径',
  `updateTime` varchar(23) DEFAULT NULL COMMENT '更新时间',
  `activeFlag` int(1) DEFAULT '1' COMMENT '记录是否可用：1-可用，0-不可用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;


-- ----------------------------
-- Table structure for p_upload_counter
-- ----------------------------
DROP TABLE IF EXISTS `p_upload_counter`;
CREATE TABLE `p_upload_counter` (
  `uploadType` varchar(50) NOT NULL,
  `counter` int(11) NOT NULL,
  `descr` varchar(50) NOT NULL,
  PRIMARY KEY (`uploadType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of p_upload_counter
-- ----------------------------
INSERT INTO `p_upload_counter` VALUES ('petAvatar', '0', '宠物头像');
INSERT INTO `p_upload_counter` VALUES ('petCard', '0', '宠物贺卡');
INSERT INTO `p_upload_counter` VALUES ('petShow', '0', '宠物写真图片');

-- ----------------------------
-- Table structure for p_users
-- ----------------------------
DROP TABLE IF EXISTS `p_users`;
CREATE TABLE `p_users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nickName` varchar(60) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '昵称',
  `openid` varchar(64) NOT NULL COMMENT '微信用户唯一识别id',
  `sessionkey` varchar(64) DEFAULT NULL COMMENT '会话秘钥',
  `avatar` varchar(255) DEFAULT NULL COMMENT '头像地址',
  `updateTime` varchar(23) DEFAULT NULL COMMENT '更新时间',
  `sex` int(1) DEFAULT NULL COMMENT '性别：1-男，2-女，0-未知',
  `petsCount` int(4) DEFAULT '0' COMMENT '拥有宠物的数量',
  `unionid` varchar(64) DEFAULT NULL COMMENT '用户在开放平台的唯一标识符',
  `language` varchar(30) DEFAULT NULL COMMENT '语言',
  `city` varchar(30) DEFAULT NULL COMMENT '城市',
  `province` varchar(30) DEFAULT NULL COMMENT '省份',
  `country` varchar(30) DEFAULT NULL COMMENT '国家',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for p_wx_run
-- ----------------------------
DROP TABLE IF EXISTS `p_wx_run`;
CREATE TABLE `p_wx_run` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `openid` varchar(64) NOT NULL,
  `step` int(11) NOT NULL DEFAULT '0' COMMENT '微信步数',
  `timestamp` bigint(20) NOT NULL COMMENT '时间戳',
  `stepRemain` int(11) DEFAULT '0' COMMENT '剩余步数',
  `feedTimes` int(2) DEFAULT '0' COMMENT '已遛狗次数',
  `feedTimesTotal` int(2) DEFAULT '3' COMMENT '总共可遛狗次数',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for p_wx_token
-- ----------------------------
DROP TABLE IF EXISTS `p_wx_token`;
CREATE TABLE `p_wx_token` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `accessToken` varchar(64) NOT NULL,
  `openid` varchar(64) NOT NULL,
  `sessionKey` varchar(64) NOT NULL,
  `updateTime` varchar(23) NOT NULL,
  `userId` bigint(20) NOT NULL COMMENT '用户id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=473 DEFAULT CHARSET=utf8;


/* 2018-01-31*/
ALTER TABLE `p_pets`
ADD COLUMN `picLikeCount`  int NULL DEFAULT 0 COMMENT '写真集点赞总数' AFTER `levelNo`;

/* 2018-02-01*/
CREATE TABLE `p_pet_card_bg` (
`id`  int UNSIGNED NOT NULL AUTO_INCREMENT ,
`cardName`  varchar(100) NULL COMMENT '贺卡名称' ,
`cardBg`  varchar(255) NULL COMMENT '贺卡背景图' ,
`updateTime`  varchar(23) NULL COMMENT '更新时间' ,
`activeFlag`  int(1) NULL DEFAULT 1 COMMENT '记录是否可用：1-可用，0-不可用' ,
PRIMARY KEY (`id`)
)
;
/* 2018-02-05*/
CREATE TABLE `p_pet_card_words` (
`id`  int UNSIGNED NOT NULL AUTO_INCREMENT ,
`cardWords`  varchar(100) NOT NULL COMMENT '贺卡词语' ,
`cardWordType`  int(2) NOT NULL COMMENT '贺卡词语类型:1-主人形容词，2-宠物形容词，3-祝福语' ,
`updateTime`  varchar(23) NULL ,
`activeFlag`  int(1) NULL COMMENT '记录是否可用：1-可用，0-不可用' ,
PRIMARY KEY (`id`)
)
;
-- init data
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('送快递的',1,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('伏地魔',1,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('躺赢的',1,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('颜值爆表',1,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('帅气的',1,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('呆萌的',1,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('可爱的',1,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('嘻哈的',1,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('跑图达人',1,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('务实的',1,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('戏精的',1,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('葛优躺的',1,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('嘴炮王者',1,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('璀璨钻石',1,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('塑料王者',1,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('富翁',1,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('吃鸡少女',1,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('一身正气的',1,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('大眼睛的',1,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('佛系',1,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('小仙女',1,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('A4腰',1,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('吴亦凡好朋友',1,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('鹿晗好朋友',1,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('老司机',1,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('油腻的',1,'2018-02-05 19:00:00',1);


insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('大胃王',2,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('长不大的',2,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('呆萌的',2,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('可爱的',2,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('蠢萌的',2,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('戏精的',2,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('表情帝',2,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('机智的',2,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('萌萌哒',2,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('机灵的',2,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('活泼的',2,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('拆家的',2,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('捣蛋的',2,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('汪界鹿晗',2,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('顽皮好动',2,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('肥头大耳',2,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('人见人爱',2,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('活泼可爱',2,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('欢蹦乱跳',2,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('古灵精怪',2,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('胖乎乎',2,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('小胖纸',2,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('小胖砸',2,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('皮皮虾',2,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('乖宝宝',2,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('汪界吴亦凡',2,'2018-02-05 19:00:00',1);


insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('把钱花光，为国争光',3,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('大吉大利，今晚吃鸡',3,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('心想事成，事与愿违',3,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('先达成一个小目标，赚它1个亿',3,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('再奋斗多一年，就有年终奖啦',3,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('世上无难事，只要肯放弃',3,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('恭喜发财，红包拿来',3,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('要想过年过得好，亲戚必须走得少',3,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('身体健康，狗年大吉',3,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('买房又买车，生活乐逍遥',3,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('狗年 旺 旺 旺',3,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('少说话，多做事，少吃零食，多睡觉',3,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('一言不合就中六合彩',3,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('阖家幸福，生活美满',3,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('新年 胖 胖 胖',3,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('全力做到最好，不如别人随便搞搞',3,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('想要梦想成真，得先从梦里醒来',3,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('化了妆，你还是心灵美的一个人',3,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('何以解忧，唯有暴富',3,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('瘦不了的永远在骚动，吃不胖的有恃无恐',3,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('不经历风雨，怎么迎接暴风雨',3,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('一起开启佛系人生',3,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('新年大吉大利，开心就好',3,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('来为你打call',3,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('2018撸起袖子加油干',3,'2018-02-05 19:00:00',1);
insert into p_pet_card_words(cardWords,cardWordType,updateTime,activeFlag) values('2018先赚个一亿',3,'2018-02-05 19:00:00',1);

/* 2018-02-06*/
ALTER TABLE `p_pets`
ADD COLUMN `nextLevelNeed`  bigint NOT NULL DEFAULT 10000 COMMENT '下一个升级目标值' AFTER `picLikeCount`;
ALTER TABLE `p_pets`
MODIFY COLUMN `levelNo`  int(3) NULL DEFAULT 0 COMMENT '等级' AFTER `activeFlag`;

