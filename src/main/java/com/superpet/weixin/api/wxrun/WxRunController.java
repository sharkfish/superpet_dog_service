package com.superpet.weixin.api.wxrun;

import com.jfinal.kit.Ret;
import com.superpet.common.controller.BaseController;
import com.superpet.common.kits.ConstantKit;
import com.superpet.common.kits.WeixinKit;
import com.superpet.common.model.WxRun;
import com.superpet.common.vo.WxSessionVo;

public class WxRunController extends BaseController {

    /**
     * 获取昨天的微信步数
     */
    public void getWeRunData(){
        String encryptedData = getPara("encryptedData");
        String ivData = getPara("iv");
        WxSessionVo wxSessionVo = getWxSession();
        wxSessionVo.setEncryptedData(encryptedData);
        wxSessionVo.setIvData(ivData);
        WxRun wxRun = WeixinKit.getWeRunData(wxSessionVo);
        if(wxRun!=null){
            renderJson(Ret.ok("code",ConstantKit.CODE_SUCCESS).set("msg",ConstantKit.MSG_SUCCESS).set("wxRun",wxRun));
        }else{
            renderJson(Ret.fail("code",ConstantKit.CODE_WX_FAIL).set("msg",ConstantKit.MSG_WX_FAIL));
        }
    }

}
