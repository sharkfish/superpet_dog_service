package com.superpet.weixin.api.User;

import com.superpet.common.kits.DateKit;
import com.superpet.common.model.Users;
import com.superpet.common.vo.WxSessionVo;

import java.math.BigInteger;
import java.util.Date;

public class UserService {

    public static final UserService me = new UserService();

    public Users getUserByOpenid(String openid){
        return Users.dao.findFirst("select * from p_users where openid=? limit 1", openid);
    }

    public Long saveOrUpdateUser(WxSessionVo wxVo){
        Users user = this.getUserByOpenid(wxVo.getOpenid());
        if(user==null){
            user = new Users();
            user.setOpenid(wxVo.getOpenid());
            user.setSessionkey(wxVo.getSession_key());
            user.setUnionid(wxVo.getUnionid());
            user.setUpdateTime(DateKit.toTimeStr(new Date()));
            user.save();
        }else{
            user.setSessionkey(wxVo.getSession_key());
            user.setUnionid(wxVo.getUnionid());
            user.setUpdateTime(DateKit.toTimeStr(new Date()));
            user.update();
        }
        return user.getId().longValue();
    }
}
